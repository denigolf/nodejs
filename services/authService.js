const UserService = require('./userService');

class AuthService {
    login(userData) {
        if (!userData.email && !userData.password) {
            userData.email = '';
            userData.password = '';
        }

        if(!userData.password) {
            throw Error('User not found');
        }

        const user = UserService.search(userData);
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }
}

module.exports = new AuthService();