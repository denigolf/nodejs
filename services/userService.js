const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getAll() {
        return UserRepository.getAll();
    }

    getOne(id) {
        return UserRepository.getOne(id); 
    }

    create(user) {

        if (!this.getAll().find(item => (item.email.toLowerCase() === user.email.toLowerCase()) || (item.phoneNumber === user.phoneNumber)  )) {
            UserRepository.create(user);
        } else {
            throw new Error('User with this email or phone number already exists!');
        }
    }

    update(id, dataToUpdate) {
        return UserRepository.update(id, dataToUpdate);
    }

    delete(id) {
        return UserRepository.delete(id);
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();