const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAll() {
        return FighterRepository.getAll();
    }

    getOne(id) {
        return FighterRepository.getOne(id);
    }

    create(fighter) {
        if (!this.getAll().find(item => (item.name.toLowerCase() === fighter.name.toLowerCase()))) {
            FighterRepository.create(fighter);
        } else {
            throw new Error("Fighter with this name already exists!")
        }
        
    }

    update(id, dataToUpdate) {
        return FighterRepository.update(id, dataToUpdate);
    }

    delete(id) {
        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();