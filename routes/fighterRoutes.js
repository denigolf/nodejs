const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.get('/', (req, res, next) => {
  res.data = FighterService.getAll();
  
  next();
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  const id = req.params.id;
  const data = FighterService.getOne({id});

  if (data) {
    res.data = data;
  } else {
    res.notFound = true;
  }

  next();
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  try {
    const data = req.body;
    res.data = data;
    if(!res.err && data) {
      FighterService.create(data);
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  } 
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
  try {
    if(!res.err) {
      const id = req.params.id;
      const data = req.body;

      const isFound = FighterService.getOne({id});

      if (isFound) {
        res.data = FighterService.update(id, data);
      } else {
        res.notFound = true;
      }
    }
  } catch (err) {
    res.err = err;
  } finally {
    next();
  } 
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  const id = req.params.id;
  const isFound = FighterService.getOne({id});

  if (isFound) {
    res.data = FighterService.delete(id);
  } else {
    res.notFound = true;
  }

  next();
}, responseMiddleware);

module.exports = router;