const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    const newFighter = req.body;
    const newFighterFields = Object.keys(newFighter);
    const requiredFields = Object.keys(fighter).filter((item) => (item !== 'health' && item !== 'id'))

    if (!newFighterFields.some((item) => Object.keys(fighter).includes(item))) {
        res.err = new Error('At least one field from model is required!');

        next();
    }

    if (!requiredFields.every((item) => newFighterFields.includes(item))) {
        res.err = new Error('All fields required except health and id!');

        next();
    } 

    if (!newFighterFields.every((item) => Object.keys(fighter).includes(item))) {
        res.err = new Error('Redundant fields are not allowed!');

        next();
    } 

    if (newFighterFields.includes('id')) {
        res.err = new Error('Id field is not allowed!');

        next();
    }  
    
    if (!isFinite(+newFighter.power) || (1 > +newFighter.power || +newFighter.power > 100)) {
        res.err = new Error('Field power must be a number (1 < power < 100)!');

        next();
    }  

    if (!isFinite(+newFighter.defense) || (1 > +newFighter.defense || +newFighter.defense > 10)) {
        res.err = new Error('Field defense must be a number (1 < defense < 10)!');

        next();
    }
    
    if (!newFighter.health) {
        newFighter.health = 100;
    }

    
    if (!isFinite(+newFighter.health) || (80 > +newFighter.health || +newFighter.health > 120) ) {
        res.err = new Error('Field health must be a number (80 < health < 120)!'); 

        next();
    } 

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const newFighter = req.body;
    const newFighterFields = Object.keys(newFighter);

    if (newFighterFields.length === 0) {
        res.err = new Error('Empty object is not allowed!');

        next();
    }

    if (!newFighterFields.every((item) => Object.keys(fighter).includes(item))) {
        res.err = new Error('Redundant fields are not allowed!');

        next();
    }

    if (newFighterFields.includes('id')) {
        res.err = new Error('Id field is not allowed!');

        next();
    }

    if (newFighter.power && (!isFinite(+newFighter.power) || (1 > +newFighter.power || +newFighter.power > 100))) {
        res.err = new Error('Field power must be a number (1 < power < 100)!');

        next();
    }

    if (newFighter.defense && (!isFinite(+newFighter.defense) || (1 > +newFighter.defense || +newFighter.defense > 10))) {
        res.err = new Error('Field defense must be a number (1 < defense < 10)!');

        next();
    }
    
    if (newFighter.health && (!isFinite(+newFighter.health) || (80 > +newFighter.health || +newFighter.health > 120)) ) {
        res.err = new Error('Field health must be a number (80 < health < 120)!');   

        next();
    } 

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;