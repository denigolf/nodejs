const { user } = require('../models/user');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const newUser = req.body;
    const newUserFields = Object.keys(newUser);
    const requiredFields = Object.keys(user).filter(item => (item !== 'id'));

    if (newUserFields.includes('id')) {
        res.err = new Error('Id field is not allowed!');

        next();
    }

    if (!newUserFields.some((item) => Object.keys(user).includes(item))) {
        res.err = new Error('At least one field from model is required!');

        next();
    }

    
    if (!requiredFields.every( (item) => newUserFields.includes(item))) {
        res.err = new Error('All fields required except id!');

        next();
    } 

    if (!newUserFields.every( (item) => Object.keys(user).includes(item))) {
        res.err = new Error('Redundant fields are not allowed!');

        next();
    } 

    if(!newUser.password || (newUser.password.length < 3)) {
        res.err = new Error('Password must be at least 3 characters!');

        next();
    }
 
    if(!newUser.email || (!newUser.email.trim().endsWith('@gmail.com'))) {
        res.err = new Error('Email must be formatted correctly (name@gmail.com)!');
        
        next();
    }
 
    if(!newUser.phoneNumber || (!newUser.phoneNumber.startsWith('+380') || newUser.phoneNumber.length !== 13)) {
        res.err = new Error('Phone number must be formatted correctly (+380xxxxxxxxx)!');
        
        next();
    }

    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const newUser = req.body;
    const newUserFields = Object.keys(newUser);

    if (newUserFields.length === 0) {
        res.err = new Error('Empty object is not allowed!')

        next();
    }

    if (newUserFields.includes('id')) {
        res.err = new Error('Id field is not allowed!');

        next();
    }  

    if (!newUserFields.every((item) => Object.keys(user).includes(item))) {
        res.err = new Error('Field is not allowed!');

        next();
    } 

    if (newUser.password && (newUser.password.length < 3)) {
        res.err = new Error('Password must be at least 3 characters!');

        next();
    }
 
    if (newUser.email && (!newUser.email.trim().endsWith('@gmail.com'))) {
        res.err = new Error('Email must be formatted correctly (name@gmail.com)!');
        
        next();
    }
 
    if (newUser.phoneNumber && (newUser.phoneNumber.startsWith('+380') || newUser.phoneNumber.length !== 13)) {
        res.err = new Error('Phone number must be formatted correctly (+380xxxxxxxxx)!');
        
        next();
    }

    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;